var request = require("request"),
  moment = require("moment");

module.exports = {
  getLatencyToGoogle: function() {
    return function(req, res) {
      var start = (new Date).getTime();
      request("http://www.google.com", function(error, response, body) {
        console.log("Request to google.com");
        if (error) {
          console.log(error);
        }
      });
      var diff = (new Date).getTime() - start;
      res.json({
        request_url: "http://www.google.com",
        latency: diff
      });
    };
  },
  setExpiresTime: function() {
    return moment(new Date()).add(10, 'minute').format("YYYY-MM-DD HH:mm:ss");
  },
  getIdType: function(user_id) {
    var self = this;
    if (self.isEmail(user_id)) {
      return 'email';
    } else if (self.isPhone(user_id)) {
      return 'phone';
    }
    return 'undefined';
  },
  isEmail: function(str) {
    var matches = str.match(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    return (matches != null && matches[0] == str) ? true : false;
  },
  isPhone: function(str) {
    var matches = str.match(/^\d+$/i);
    return (matches != null && matches[0] == str) ? true : false;
  },
  requestToken: function(id, password, res) {
    request.post({
      url: 'http://localhost:3000/token',
      headers: {
        'Content-Type': "application/x-www-form-urlencoded"
      },
      form: {
        grant_type: 'password',
        id: id,
        password: password
      }
    }, function(err, httpResponse, body) {
      res.json(JSON.parse(body));
    })
  },
  ErrorBadRequest: function(res) {
    res.status(400);
    res.header("WWW-Authenticate", 'Bearer realm="Digicode", error="invalid_request"');
    res.json({
      error: "invalid_request"
    });
  }
};