var path = require("path");
var env = process.env.NODE_ENV || "development";
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
var Sequelize = require('sequelize');

//init database connection
var sequelize = new Sequelize(config.database, config.username, config.password, {
	timezone: '+03:00'
});

// load models
var models = [
	'user',
	'token',
];
models.forEach(function(model) {
	module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// export connection
module.exports.sequelize = sequelize;