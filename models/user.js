var helpers = require('../helpers/helpers.js');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.STRING(60),
      primaryKey: true,
      unique: true,
      validate: {
        isValide: function(value) {
          if (helpers.getIdType(value) == 'undefined') {
            throw new Error('Invalide user id type!')
          }
        }
      }
    },
    password: DataTypes.STRING(60),
    id_type: {
      type: DataTypes.ENUM('email', 'phone')
    }
  }, {
    classMethods: {
      getData: function(req, res, id) {
        return this.findOne({
          where: {
            id: id
          }
        }).then(function(user) {
          if (user == null) {
            helpers.ErrorBadRequest(res);
          } else {
            res.json({
              id: user.id,
              type: user.id_type
            });
          }
        });
      }
    }
  });
};