var helpers = require('../helpers/helpers.js');
var randtoken = require('rand-token');
var moment = require('moment');

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('token', {
		user_id: {
			type: DataTypes.STRING(60),
			references: {
				model: "users",
				key: "id"
			}
		},
		token: DataTypes.STRING,
		expires_in: DataTypes.DATE
	}, {
		classMethods: {
			getToken: function(user_id, res) {
				return this.findOne({
					where: {
						user_id: user_id,
						expires_in: {
							$gt: new Date()
						}
					}
				}).then(function(token) {
					if (token == null) {
						this.createToken(user_id, res);
					} else {
						res.json({
							access_token: token.token,
							token_type: "bearer"
						});
					}
				});
			},
			createToken: function(user_id, res) {
				return this.create({
					user_id: user_id,
					token: randtoken.generate(64),
					expires_in: moment(new Date()).add(10, 'minute').format("YYYY-MM-DD HH:mm:ss")
				}).then(function(token) {
					if (token == null) {
						helpers.ErrorBadRequest(res);
					} else {
						res.json({
							access_token: token.token,
							token_type: "bearer"
						});
					}
				});
			},
			prolongToken: function(token, next) {
				return this.find({
					where: {
						token: token
					}
				}).then(function(token) {
					if (token) {
						token.update({
							expires_in: moment(new Date()).add(10, 'minute').format("YYYY-MM-DD HH:mm:ss")
						}).then(function() {
							next();
						})
					}
				});
			}
		}
	});
};