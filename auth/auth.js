var helpers = require('../helpers/helpers.js');
var Token = require('../models/index.js').token,
	User = require('../models/index.js').user;

module.exports = {
	authorize: function() {
		return function(req, res, next) {
			//check if token valide
			Token.findOne({
				where: {
					token: req.token,
					expires_in: {
						$gt: new Date()
					}
				}
			}).then(function(token) {
				if (token == null) {
					res.status(401);
					res.header("WWW-Authenticate", 'Bearer realm="Digicode", error="invalid_token"');
					res.json({
						error: "invalid_token"
					});
				} else {
					Token.prolongToken(req.token, next);
				}
			});
		};
	},
	authenticate: function() {
		return function(req, res, next) {
			if (typeof req.body.id == 'undefined' ||
				typeof req.body.password == 'undefined' ||
				Object.keys(req.body).length != 2) {
				res.status(400);
				res.json({
					error: "invalid_request"
				});
			} else {
				var type = helpers.getIdType(req.body.id); // email,phone or underfined
				if (type == 'undefined') {
					helpers.ErrorBadRequest(res);
				} else {
					User.findOne({
						where: {
							id: req.body.id
						}
					}).then(function(user) {
						if (user == null) {
							if (req.path == '/signin') {
								helpers.ErrorBadRequest(res);
							}
							next();
						} else {
							helpers.requestToken(req.body.id, req.body.password, res);
						}
					});
				}
			}
		}
	},
	logout: function() {
		return function(req, res) {
			if (req.query.all == 'true') {
				Token.findOne({
					where: {
						token: req.token
					}
				}).then(function(token) {
					if (token) {
						Token.destroy({
							where: {
								user_id: token.user_id
							}
						}).then(function() {
							res.redirect('/login');
						});
					}
				});

			} else if (req.query.all == 'false') {
				Token.destroy({
					where: {
						token: req.token
					}
				}).then(function() {
					res.redirect('/login');
				});
			} else {
				helpers.ErrorBadRequest(res);
			}
		};
	}
}