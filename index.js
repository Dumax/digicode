var express = require('express');
var app = express();
var sequelize = require('./models/index.js').sequelize;
var helpers = require('./helpers/helpers.js');
var auth = require('./auth/auth.js');
var request = require('request');
/*
 * Models
 */
var Token = require('./models/index.js').token,
	User = require('./models/index.js').user;
/*
 * Middlewares
 */
var cors = require('cors'),
	bodyParser = require("body-parser"),
	bearerToken = require('express-bearer-token');

//enable all CORS Requests
app.use(cors());
// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({
	extended: false
}));
// parse application/json 
app.use(bodyParser.json());
//get token from Authorization header
app.use(bearerToken());

/*
 * Routes
 */
app.post('/signin', auth.authenticate());
app.post('/signup', auth.authenticate(), function(req, res) {
	var type = helpers.getIdType(req.body.id);
	User.create({
		id: req.body.id,
		password: req.body.password,
		id_type: type
	}).then(function(user) {
		if (user) {
			Token.createToken(user.id, res);
		}
	});
});
app.get('/info', auth.authorize(), function(req, res) {
	Token.findOne({
		where: {
			token: req.token
		}
	}).then(function(token) {
		User.getData(req, res, token.user_id);
	});
});
app.get('/latency', auth.authorize(), helpers.getLatencyToGoogle());
app.get('/logout', auth.authorize(), auth.logout());
app.post('/token', function(req, res, next) {
	if (req.get('Content-Type') == 'application/x-www-form-urlencoded; charset=utf-8' &&
		req.body.grant_type == 'password') {
		User.findOne({
			where: {
				id: req.body.id,
				password: req.body.password
			}
		}).then(function(user) {
			if (user) {
				Token.getToken(user.id, res);
			} else {
				helpers.ErrorBadRequest(res);
			}
		});
	} else {
		helpers.ErrorBadRequest(res);
	}
});

sequelize.sync().then(function() {
	app.listen(3000, function() {
		console.log('Example app listening on port 3000!');
	});
});